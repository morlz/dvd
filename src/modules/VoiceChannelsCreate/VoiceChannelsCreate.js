import { Module } from '@/lib/core'
import { Permissions } from 'discord.js'
import app from 'app'
import db from '@/lib/db'
import uuid from 'uuid/v4'
import { Collection } from 'discord.js'


export default class VoiceChannelsCreate extends Module {
	constructor () {
		super ()

		this.groups = new Collection()
		this.ccs = new Collection()
		this.roles = new Collection()
		this.running = false
	}

	install (_app) {
		super.install(_app)

		this.__addEvents()
	}

	async __addEvents () {
		const { discordClient } = this._app

		discordClient.on('ready', async () => {
			this.emit('log', 'init start')

			/*
			await Promise.all(
				discordClient.guilds.map(async guild => {
					await Promise.all(
						guild.channels.map(ch => ch.delete())
					)

					for (let i = 0; i < 100; i++) {

						await Promise.all(
							Array(10).fill(0).map((e, index) => {
								return guild.createChannel(i * 10 + index, 'text')
							})
						)
					}
				})
			)
			*/




			try {
				await this.__initGroups()
			} catch (err) {
				console.error('Error while init groups', err)
			}

			try {
				await this.__initCCs()
			} catch (err) {
				console.error('Error while init ccs', err)
			}

			try {
				await this.__initRoles()
			} catch (err) {
				console.error('Error while init roles', err)
			}

			try {
				await this.__initCCLoop()
			} catch (err) {
				console.error('Error while init loop', err)
			}

			this.emit('log', 'init complete')
		})
	}

	async __initGroups (guild) {
		this.emit('log', 'init groups')
		const guilds = await db.dvd.collection('guilds')
		const groups = await guilds
			.find()
			.toArray()

		groups.map(group => this.groups.set(group.id, group))
	}

	async __initCCs () {
		this.emit('log', 'init ccs')
		const { discordClient } = this._app
		const ccsCollection = await db.dvd.collection('ccs')
		const ccs = await ccsCollection
			.find()
			.toArray()

		ccs.map(cc => this.ccs.set(cc.guildID, cc.ccID))

		await Promise.all(
			discordClient.guilds
				.map(guild => this.__ensureCC(guild))
		)
	}

	async __initRoles () {
		this.emit('log', 'init roles')
		const { discordClient } = this._app
		const rolesCollection = await db.dvd.collection('roles')
		const roles = await rolesCollection
			.find()
			.toArray()

		roles.map(cc => this.roles.set(cc.guildID, cc.roleID))

		await Promise.all(
			discordClient.guilds
				.map(guild => this.__ensureMemberRole(guild))
		)
	}

	__initCCLoop () {
		this.emit('log', 'init cc loop')
		const { discordClient } = this._app

		setInterval(async () => {
			if (this.running) return

			this.running = true

			await Promise.all(
				this.ccs.map(async (ccID, guildID) => {
					try {
						const guild = discordClient.guilds.get(guildID)
						if (!guild)
							return await this.__removeCCFromDB(guildID)

						const channelCC = await this.__ensureCC(guild)
						await Promise.all(
							channelCC.members
								.map(member => this.__handleCCMember(member))
						)
					} catch (err) {
						console.error('Error while handle cc member', err)
					}
				})
			)

			await Promise.all(
				this.groups
					.map(async group => {
						try {
							return await this.__handleGroupVisible(group)
						} catch (err) {
							console.error('Error while handle groups visible', err)
						}
					})
			)

			this.running = false
		}, 1e3)
	}

	async __handleGroupVisible (group) {
		try {
			const { discordClient } = this._app
			const voice = discordClient.channels.get(group.voiceID)
			if (!voice)
				return await this.__repairGroup(group)

			const { size } = voice.members
			if ((group.visible && size) || (!group.visible && !size)) return

			await this.__setGroupVisible(group, !!size)
		} catch (err) {
			console.error('Error while __handleGroupVisible', err)
		}
	}

	async __setGroupVisible (group, value = false) {
		const { discordClient } = this._app
		const guild = discordClient.guilds.get(group.guildID)
		const roleMember = await this.__ensureMemberRole(guild)
		const everyone = this.__getEvryone(guild)
		const channelCCID = this.ccs.get(group.guildID)
		const channelCC = discordClient.channels.get(channelCCID)
		const groupMember = guild.members.get(group.memberID)
		const channels = [
				group.categoryID,
				group.textID,
				group.voiceID
			]
			.map(channelID => discordClient.channels.get(channelID))
			.filter(el => el)

		if (channels.length != 3)
			return await this.__repairGroup(group)

		await Promise.all(
			[
				channelCC.overwritePermissions(groupMember, {
					VIEW_CHANNEL: !value
				}),
				...channels.map(
					channel => channel.overwritePermissions(roleMember, {
						VIEW_CHANNEL: value,
						SEND_MESSAGES: value
					})
				),
				...channels.map(
					channel => channel.overwritePermissions(everyone, {
						VIEW_CHANNEL: value,
						SEND_MESSAGES: value
					})
				)
			]
		)

		group.visible = value

		this.emit('log', `${groupMember} ${group.id} set visible to ${value}`)
	}

	async __handleCCMember (member) {
		const group = await this.__ensureGroup(member)
		member.setVoiceChannel(group.voiceID)
		this.emit('log', `${member} move to ${group.voiceID}`)
	}

	async __ensureCC (guild) {
		const { discordClient } = this._app
		const ccID = this.ccs.get(guild.id)

		if (ccID) {
			const channelCC = discordClient.channels.get(ccID)

			if (channelCC)
				return channelCC
		}

		return await this.__createCC(guild)
	}

	async __createCC (guild) {
		this.emit('log', `${guild} create cc`)
		const channelCC = await guild.createChannel('create-channel', 'voice')
		const ccs = await db.dvd.collection('ccs')
		const { ops } = await ccs.insertOne({
			guildID: guild.id,
			ccID: channelCC.id,
		})

		this.ccs.set(guild.id, channelCC.id)

		this.emit('log', `${guild} created cc ${channelCC}`)

		return channelCC
	}

	async __ensureGroup (member) {
		const group = this.groups.find(group => {
			return group.memberID == member.id
				&& group.guildID == member.guild.id
		})

		if (group)
			return group

		return this.__createGroup(member)
	}

	async __createGroup (member) {
		const { guild } = member
		this.emit('log', `${member} in ${guild} create group`)

		const roleMember = await this.__ensureMemberRole(guild)
		const everyone = this.__getEvryone(guild)

		const invisiblePerms = [
			{
				id: roleMember,
				deny: ['VIEW_CHANNEL']
			},
			{
				id: everyone,
				deny: ['VIEW_CHANNEL']
			},
			{
				id: member,
				allow: ['MANAGE_ROLES', 'MANAGE_MESSAGES']
			}
		].filter(el => el.id)



		const [category, voice, text] = await Promise.all([
			guild.createChannel(member.user.username, 'category', invisiblePerms),
			guild.createChannel('voice', 'voice', invisiblePerms),
			guild.createChannel('text', 'text', invisiblePerms)
		])

		await Promise.all(
			[voice, text].map(channel => channel.setParent(category.id))
		)

		const guilds = await db.dvd.collection('guilds')
		const { ops } = await guilds.insertOne({
			id: uuid(),
			guildID: member.guild.id,
			memberID: member.id,
			textID: text.id,
			voiceID: voice.id,
			categoryID: category.id,
			visible: false
		})

		const group = ops[0]

		this.groups.set(group.id, group)

		this.emit('log', `${member} in ${guild}  created group ${group.id}`)

		return group
	}


	async __ensureMemberRole (guild) {
		const roleID = this.roles.get(guild.id)
		if (roleID) {
			const role = guild.roles.get(roleID)

			if (role)
				return role
		}

		const role = await guild.createRole({
			name: 'Участник'
		})

		const roles = await db.dvd.collection('roles')
		const { ops } = await roles.insertOne({
			guildID: guild.id,
			roleID: role.id
		})

		this.roles.set(guild.id, role.id)

		this.emit('log', `In ${guild}  created role member ${role}`)

		return role
	}

	__getEvryone (guild) {
		return guild.roles.find(el => el.name == '@everyone')
	}

	async __removeCCFromDB (guildID) {
		const ccs = await db.dvd.collection('ccs')
		await ccs.deleteOne({
			guildID
		})
		this.ccs.delete(guildID)
	}

	async __repairGroup (group) {
		try {
			const { discordClient } = this._app

			const channels = [
					group.categoryID,
					group.textID,
					group.voiceID
				]
				.map(channelID => discordClient.channels.get(channelID))
				.filter(el => el)

			await Promise.all(
				channels.map(channel => channel.delete())
			)

			const groups = await db.dvd.collection('guilds')
			await groups.deleteOne({
				id: group.id
			})
			this.groups.delete(group.id)

			const guild = discordClient.guilds.get(group.guildID)

			if (!guild)
				return

			const member = guild.members.get(group.memberID)

			if (!member)
				return

			const channelCC = await this.__ensureCC(guild)

			if (channelCC)
				channelCC.overwritePermissions(member, {
					VIEW_CHANNEL: true
				})

			await this.__createGroup(member)
		} catch (err) {
			console.error('Error while group repair', err)
		}
	}
}
