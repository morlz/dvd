import { Module } from '@/lib/core'
import fs from 'fs-extra'

export default class Config extends Module {
	constructor () {
		super()
		this._config = {}

		return new Proxy(this, {
			get: (self, prop) =>
				prop in self ?
					self[prop]
				:	self._config[prop],
			set: (self, prop, value) => {
				self[prop] = value
				return true
			}
		})
	}

	async install (app) {
		super.install(app)
		await this.__readConfig()
	}

	async __readConfig () {
		this.emit('log', 'read config')
		let configStr = await fs.readFile('./config.json', { encoding: 'utf-8' })
		this._config = JSON.parse(configStr)
		this.emit('log', 'config readed')
	}
}
