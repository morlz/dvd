import { Module } from '@/lib/core'
import { Client } from 'discord.js'

import app from 'app'

export default class DiscordClient extends Module {
	constructor () {
		super ()

		this.client = new Client()

		return new Proxy(this.client, {
			get: (self, prop) => prop in self ?
				self[prop]
			:	this[prop],

			set: (self, prop, value) =>
				this[prop] = value
		})
	}

	install (_app) {
		super.install(_app)

		this._addEvents()

		this.login(this._app.config.token)
	}

	_addEvents () {
		this.on('ready', a => this.emit('log', 'ready'))
		this.on('message', msg => this.emit('log', 'message', msg.content))
	}

	get guild () {
		if (!this.client.guilds.has(this._app.config.createChannel.id))
			throw new Error('Нет доступа к каналу ' + this._app.config.createChannel.id)

		return this.client.guilds.get(this._app.config.createChannel.id)
	}
}
