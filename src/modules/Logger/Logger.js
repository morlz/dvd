import { Module } from '@/lib/core'

export default class Logger extends Module {
	constructor () {
		super()

		this._loggers = new Proxy({}, {
			set: (self, prop, value) => {
				this.emit('logger-create', {
					name: prop,
					fn: value
				})

				self[prop] = value
				return true
			}
		})
	}

	install (app) {
		super.install(app)
		this._app.on('module-installed', this.__addEvents.bind(this))
	}

	__addEvents ({ className, instance }) {
		instance.on('log', this.__createLogger(className))
	}

	__createLogger (name) {
		this._loggers[name] = (...args) => {
			console.log(`[${name}]`, ...args)
			this.emit('logger-log', { name, args })
		}

		return this._loggers[name]
	}
}
