import 'babel-polyfill'
import { App } from '@/lib/core'
import { connect } from '@/lib/db'
import * as modules from '@/modules'

void async function () {
    await connect()

    new App(modules)
}()
