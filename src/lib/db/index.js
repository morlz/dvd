import { MongoClient } from 'mongodb'

const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'dvd'

// Create a new MongoClient
const client = new MongoClient(url, { useNewUrlParser: true })
let dvd = null


Object.defineProperty(client, 'dvd', {
    get () {
        return dvd
    }
})

async function connect () {
    return new Promise((resolve, reject) => {
        client.connect(function(err) {
            if (err)
                reject(err)


            dvd = client.db('dvd')
            resolve()
        })
    })
}

export default client
export {
    connect
}
