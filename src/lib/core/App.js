import ModuleLoader from './ModuleLoader'

export default class App extends ModuleLoader {
	constructor (modules = {}) {
		super()
		this.on('module-installed', ({ name }) => console.log(`Module ${name} installed`))
		this.installModules(modules)
	}
}
