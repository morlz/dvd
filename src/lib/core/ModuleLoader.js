const EventEmitter = require('events')
import { Module } from '@/lib/core'

export default class ModuleLoader extends EventEmitter {
	constructor () {
		super()
	}

	async installModules(modules = {}) {
		if (typeof modules !== 'object')
			throw new Error('Modules must be an object')

		for (let name of Object.keys(modules))
			await this.__installModule(name, modules[name])
	}

	async __installModule (className, NewModule) {
		let name = className.charAt(0).toLowerCase() + className.slice(1)

		if (NewModule.prototype instanceof Module === false)
			throw new Error('modules must implementate lib/core/Module')

		this[name] = new NewModule()
		await this[name].install(this)

		this.emit('module-installed', {
			instance: this[name],
			name,
			className
		})
	}
}
