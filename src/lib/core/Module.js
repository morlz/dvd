const EventEmitter = require('events')

export default class Module extends EventEmitter {
	install (app) {
		this._app = app
	}
}
